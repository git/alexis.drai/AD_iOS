//
//  PodcastEpisode.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-21.
//

import Foundation
struct Episode {
    var id: UUID
    var publicationDate: Date
    var title: String
    var description: String
    var duration: TimeInterval // duration in seconds
}
