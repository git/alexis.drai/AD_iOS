//
//  Podcast.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-16.
//

import Foundation
import SwiftUI

struct Podcast {
    var id: UUID
    var image: UIImage
    var title: String
    var by: String
    private var _episodes: [Episode]
    var rating: Double
    var reviews: Int
    var genre: String
    var frequency: String
    var backgroundColor: Color
    var backgroundIsDark: Bool
    
    init(
        id: UUID,
        image: UIImage,
        title: String,
        by: String,
        episodes: [Episode],
        rating: Double,
        reviews: Int,
        genre: String,
        frequency: String = "Unknown",
        backgroundColor: Color = Color.theme.background,
        backgroundIsDark: Bool = false
    ) {
        self.id = id
        self.image = image
        self.title = title
        self.by = by
        self._episodes = episodes
        self.rating = rating
        self.reviews = reviews
        self.genre = genre
        self.frequency = frequency
        self.backgroundColor = backgroundColor
        self.backgroundIsDark = backgroundIsDark
    }
    
    
    var episodes: [Episode] {
        _episodes.sorted(by: { $0.publicationDate > $1.publicationDate })
    }
    
    var latestEpisodeDescription: String {
        guard !episodes.isEmpty else { return "No episodes available" }
        return episodes.first?.description ?? "No description available"
    }
}
