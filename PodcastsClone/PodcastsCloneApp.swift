//
//  PodcastsCloneApp.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-12.
//

import SwiftUI

@main
struct PodcastsCloneApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
