//
//  NowPlayingBar.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-21.
//

// Thanks to Luca Jonscher for this tutorial: https://itnext.io/add-a-now-playing-bar-with-swiftui-to-your-app-d515b03f05e3
// This bar is almost all made by them. The mistakes are all mine.
import SwiftUI

struct NowPlayingBar<Content: View>: View {
    var content: Content
    
    @ViewBuilder var body: some View {
        ZStack {
            Blur(style: .systemMaterial) // Use Blur view as background
                .frame(width: UIScreen.main.bounds.size.width, height: 64)
            
            VStack {
                HStack {
                    Button(action: {}) {
                        HStack {
                            Image("jjho_logo")
                                .resizable()
                                .frame(width: 48, height: 48)
                                .shadow(radius: 4, x: 0, y: 2)
                                .padding(.leading)
                            VStack(alignment: .leading) {
                                Text("Acting in Bat Faith")
                                    .foregroundColor(Color.theme.primary)
                                Text("1 February 2023")
                                    .font(.caption)
                                    .foregroundColor(Color.theme.secondary)
                            }.padding()
                            Spacer()
                        }
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    Button(action: {}) {
                        Image(systemName: "play.fill")
                            .font(.title3)
                    }
                    .buttonStyle(PlainButtonStyle())
                    .padding(.horizontal)
                    
                    Button(action: {}) {
                        Image(systemName: "gobackward.30")
                            .font(.title3)
                    }
                    .buttonStyle(PlainButtonStyle())
                    .padding(.trailing, 30)
                }
            }
        }
    }
}


struct Blur: UIViewRepresentable {
    var style: UIBlurEffect.Style = .systemChromeMaterial
    
    func makeUIView(context: Context) -> UIVisualEffectView {
        return UIVisualEffectView(effect: UIBlurEffect(style: style))
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        uiView.effect = UIBlurEffect(style: style)
    }
}

struct NowPlayingBar_Previews: PreviewProvider {
    static var previews: some View {
        NowPlayingBar(content: Text("Hello Bar"))
    }
}
