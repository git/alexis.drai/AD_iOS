//
//  PodcastEpisodeViewCell.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-12.
//

import SwiftUI


struct EpisodeViewCell: View {
    
    let episode: Episode
    
    var body: some View {
        VStack {
            Divider()
                .foregroundColor(Color.theme.backgroundSecondary)
                .edgesIgnoringSafeArea(.trailing)
                .padding(.leading)
            
            VStack(alignment: .leading) {

                Text(formatter.localizedString(for: episode.publicationDate, relativeTo: Date()))
                    .font(.subheadline)
                    .foregroundColor(Color.theme.secondary)
            
                (Text(episode.title)
                    .font(.headline)
                    .foregroundColor(Color.theme.primary)
                + Text("\n\(episode.description)")
                    .font(.body)
                    .foregroundColor(Color.theme.secondary))
                .lineLimit(4)
                .truncationMode(.tail)
                
                HStack {
                    Image(systemName: "play.fill")
                        .foregroundColor(Color.theme.accent)
                        .padding()
                        .background(Color.theme.backgroundSecondary)
                        .clipShape(Circle())
                    Text(timeString(time: episode.duration))
                        .foregroundColor(Color.theme.accent)
                    Spacer()
                    Text(Strings.threeDots)
                        .foregroundColor(Color.theme.secondary)
                        .padding(.horizontal)
                }
            }
            .padding()
        }
    }
    
    private func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60

        var timeComponents = [String]()
        if hours > 0 {
            timeComponents.append("\(hours) hr")
        }
        if minutes > 0 {
            timeComponents.append("\(minutes) min")
        }

        return timeComponents.joined(separator: " ")
    }
    
    private let formatter: RelativeDateTimeFormatter = {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter
    }()
    
}

struct EpisodeViewCell_Previews: PreviewProvider {
    
    static var previews: some View {
        EpisodeViewCell(episode: Stub.episodes[1])
    }
}
