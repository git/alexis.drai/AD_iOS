//
//  Color.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-21.
//

import Foundation
import SwiftUI

extension Color {
    static let theme = ColorTheme()
}

struct ColorTheme {
    let primary = Color("primary")
    let secondary = Color("secondary")
    let background = Color("background")
    let backgroundSecondary = Color("backgroundSecondary")
    let accent = Color("accent")
    let shadow = Color("shadow")
    let unchangingPrimaryDark = Color("primaryDark")
    let unchangingPrimaryLight = Color("primaryLight")
    let unchangingSecondaryDark = Color("secondaryDark")
    let unchangingSecondaryLight = Color("secondaryLight")
    let unchangingBackgroundDark = Color("backgroundDark")
    let unchangingBackgroundLight = Color("backgroundLight")
}
