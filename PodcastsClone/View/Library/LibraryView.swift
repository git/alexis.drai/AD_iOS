//
//  LibraryView.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-16.
//

import SwiftUI

struct LibraryView: View {
    
    var podcasts: [Podcast]
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: .leading) {
                    Text("Podcasts")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .foregroundColor(Color.theme.primary)
                        .padding()
                        
                    let columns = [
                        GridItem(.flexible(), spacing: 16),
                        GridItem(.flexible(), spacing: 16)
                    ]

                    LazyVGrid(columns: columns, spacing: 16) {
                        ForEach(podcasts, id: \.id) { podcast in
                            NavigationLink(
                                destination: PodcastDetailView(podcast: podcast)) {
                                    PodcastViewCell(podcast: podcast)
                            }
                        }
                    }
                    .padding(.horizontal)
                }
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct LibraryView_Previews: PreviewProvider {
    static var previews: some View {
        LibraryView(podcasts: Stub.podcasts)
    }
}
