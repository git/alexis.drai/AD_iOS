//
//  MainView.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-12.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        ZStack {
            
            TabView {
                Text("Check out the library instead")
                    .tabItem {
                        Label("Listen Now", systemImage: "play")
                    }
                Text("I swear, the library is where it's at")
                    .tabItem {
                        Label("Browse", systemImage: "square.grid.2x2")
                    }
                LibraryView(podcasts: Stub.podcasts)
                    .tabItem {
                        Label("Library", systemImage: "book")
                    }
                Text("Nothing to see here. The library, on the other hand...")
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass")
                    }
            }
            .accentColor(Color.theme.accent)
            .tabViewStyle(.automatic)
            
            VStack {
                Spacer()
                VStack{
                    NowPlayingBar(content: Text("Gahhh!"))
                }
                .offset(y: -42)
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
