//
//  PodcastDetailView.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-12.
//

import SwiftUI

struct PodcastDetailView: View {
    
    let podcast: Podcast
    
    var body: some View {
        ScrollView {
            ZStack() {
                podcast.backgroundColor.ignoresSafeArea(.all, edges: .all)
                VStack(alignment: .center) {
                    VStack(alignment: .center) {

                        Image(uiImage: podcast.image)
                            .resizable()
                            .scaledToFit()
                            .cornerRadius(12)
                            .shadow(color: Color.theme.unchangingPrimaryLight, radius: 10, x: 0, y: 5)
                            .padding(.horizontal, 48)
                            .padding(.vertical, 16)
                        
                        Text(podcast.title)
                            .font(.title)
                            .foregroundColor(podcast.backgroundIsDark ? Color.theme.unchangingPrimaryDark : Color.theme.unchangingPrimaryLight)
                            .multilineTextAlignment(.center)
                        
                        Text(podcast.by)
                            .font(.headline)
                            .foregroundColor(podcast.backgroundIsDark ? Color.theme.unchangingSecondaryDark : Color.theme.unchangingSecondaryLight)
                            .multilineTextAlignment(.center)
                        
                        Button(action: {}) {
                            HStack {
                                Image(systemName: "play.fill")
                                    .padding(.horizontal, 4)
                                Text(Strings.latestEpisode)
                            }}
                            .padding(.vertical)
                            .padding(.horizontal, 64)
                            .background(podcast.backgroundIsDark ? Color.theme.unchangingBackgroundLight : Color.theme.unchangingBackgroundDark)
                            .foregroundColor(podcast.backgroundIsDark ? Color.theme.unchangingPrimaryLight : Color.theme.unchangingPrimaryDark)
                            .clipShape(RoundedRectangle(cornerSize: CGSize(width: 12.0, height: 12.0)))
                        
                        // TODO replace '...' with Strings.readFurtherPrompt
                        Text(podcast.latestEpisodeDescription)
                            .lineLimit(3)
                            .truncationMode(.tail)
                            .padding()
                            .foregroundColor(podcast.backgroundIsDark ? Color.theme.unchangingPrimaryDark : Color.theme.unchangingPrimaryLight)
                        
                        HStack() {
                            Text("\(Image(systemName: "star.fill")) \(podcast.rating, specifier: "%.1f") (\(podcast.reviews)) \(Strings.classySeparator) \(podcast.genre) \(Strings.classySeparator) \(podcast.frequency)")
                                .padding(.horizontal)
                            
                            Spacer()
                        }
                        .foregroundColor(podcast.backgroundIsDark ? Color.theme.unchangingSecondaryDark : Color.theme.unchangingSecondaryLight)
                    }

                    Divider()
                        .foregroundColor(Color.theme.backgroundSecondary)

                    ZStack() {
                        Color.theme.background.ignoresSafeArea(.all, edges: .all)
                        
                        VStack(alignment: .leading) {
                            HStack {
                                Text(Strings.episodes)
                                    .font(.title2)
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.theme.primary)
                                    .padding()
                                
                                Image(systemName: "chevron.down")
                                    .foregroundColor(Color.theme.accent)
                                
                                Spacer()
                                
                                Text(Strings.seeAll)
                                    .foregroundColor(Color.theme.accent)
                                    .padding(.trailing)
                            }
                            
                                          
                            ForEach(podcast.episodes, id: \.id) { episode in
                                EpisodeViewCell(episode: episode)
                            }
                        }
                    }
                }
            }
        }
    }
}


struct PodcastDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PodcastDetailView(podcast: Stub.podcasts[2])
        PodcastDetailView(podcast: Stub.podcasts[2])
    }
}
