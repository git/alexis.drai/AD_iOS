//
//  PodcastViewCell.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-21.
//

import SwiftUI

struct PodcastViewCell: View {
    let podcast: Podcast
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(uiImage: podcast.image)
                .resizable()
                .scaledToFit()
                .cornerRadius(12)
            
            Text(podcast.title)
                .foregroundColor(Color.theme.primary)
            
            Text("Updated \(smartDate(podcast.episodes.first?.publicationDate ?? Date()))") .foregroundColor(Color.theme.secondary)
                .font(.footnote)
        }
    }
    
    func smartDate(_ date: Date) -> String {
        let calendar = Calendar.current
        let now = Date()
        let components = calendar.dateComponents([.day, .year], from: date, to: now)
        
        if let day = components.day {
            switch day {
            case 0:
                return "Today"
            case 1..<7:
                return "\(day)d ago"
            default:
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMMM yyyy"
                return formatter.string(from: date)
            }
        } else {
            return "Unknown"
        }
    }
}

struct PodcastViewCell_Previews: PreviewProvider {
    static var previews: some View {
        PodcastViewCell(podcast: Stub.podcasts[1])
    }
}
