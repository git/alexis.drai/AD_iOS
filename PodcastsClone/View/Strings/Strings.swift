//
//  Strings.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-21.
//

import Foundation

struct Strings {
    static let threeDots = "···"
    static let classySeparator = "·"
    static let latestEpisode = "Latest Episode"
    static let readFurtherPrompt = "MORE"
    static let episodes = "Episodes"
    static let seeAll = "See All"
}
