//
//  Stub.swift
//  PodcastsClone
//
//  Created by etudiant on 2023-05-21.
//

import Foundation
import SwiftUI

struct Stub {
    static let episodes: [Episode] = [
        Episode(
            id: UUID(),
            publicationDate: Date.now.addingTimeInterval(-1000000000),
            title: "A New Ipsum",
            description: "Stand in doorway, unwilling to chose whether to stay in or go out more napping, more napping all the napping is exhausting sleep i'm bored inside, let me out i'm lonely outside, let me in i can't make up my mind whether to go in or out, guess i'll just stand partway in and partway out, contemplating the universe for half an hour how dare you nudge me with your foot?!?! leap into the air in greatest offense!",
            duration: 3463
        ),
        Episode(
            id: UUID(),
            publicationDate: Date.now.addingTimeInterval(-1000000),
            title: "Return of the Hooman",
            description: "Catch mouse and gave it as a present mewl for food at 4am drink water out of the faucet and have secret plans. Stretch chase dog then run away. Kitty. Mouse if it fits, i sits. Bite off human's toes. If human is on laptop sit on the keyboard.",
            duration: 4480
        ),
        Episode(
            id: UUID(),
            publicationDate: Date.now.addingTimeInterval(-100000000),
            title: "Cat Ipsum Strikes Back",
            description: "Chase after silly colored fish toys around the house i want to go outside let me go outside nevermind inside is better or get video posted to internet for chasing red dot eat owner's food wack the mini furry mouse so cat meoooow i iz master of hoomaan, not hoomaan master of i, oooh damn dat dog but stuff and things. Cats making all the muffins.",
            duration: 4028
        ),
    ]
    
    static let episodesOld = Stub.episodes.map { episode in
        Episode(
            id: episode.id,
            publicationDate: Date.now.addingTimeInterval((episode.publicationDate.timeIntervalSinceNow - Date.now.timeIntervalSinceNow) * 10),
            title: episode.title,
            description: episode.description,
            duration: episode.duration
        )
    }
    
    static let episodesRecent = Stub.episodes.map { episode in
        Episode(
            id: episode.id,
            publicationDate: Date.now.addingTimeInterval((episode.publicationDate.timeIntervalSinceNow - Date.now.timeIntervalSinceNow) / 10.0),
            title: episode.title,
            description: episode.description,
            duration: episode.duration
        )
    }
    
   static let podcasts: [Podcast] = [
        Podcast(
            id: UUID(),
            image: UIImage(named: "jjho_logo")!,
            title: "Podcast Title 1",
            by: "Author 1",
            episodes: episodesOld,
            rating: 4.2,
            reviews: 2139,
            genre: "Genre 1",
            frequency: "Weekly",
            backgroundColor: Color("jjhoColor"),
            backgroundIsDark: true
        ),
        Podcast(
            id: UUID(),
            image: UIImage(named: "jjgo_logo")!,
            title: "Podcast Title 2",
            by: "Author 2",
            episodes: episodes,
            rating: 4.2,
            reviews: 211,
            genre: "Genre 2",
            frequency: "Twice weekly",
            backgroundColor: Color("jjgoColor"),
            backgroundIsDark: true
        ),
        Podcast(
            id: UUID(),
            image: UIImage(named: "spy_logo")!,
            title: "Podcast Title 3",
            by: "Author 3",
            episodes: episodesRecent,
            rating: 4.812039,
            reviews: 3981,
            genre: "Genre 3",
            frequency: "Complete",
            backgroundColor: Color("spyColor"),
            backgroundIsDark: true
        ),
        Podcast(
            id: UUID(),
            image: UIImage(named: "bdnp_logo")!,
            title: "Podcast Title 4",
            by: "Author 4",
            episodes: episodesOld,
            rating: 4.2,
            reviews: 211,
            genre: "Genre 4",
            frequency: "Monthly",
            backgroundColor: Color("bdnpColor"),
            backgroundIsDark: false
        ),
        Podcast(
            id: UUID(),
            image: UIImage(named: "bewjt_logo")!,
            title: "Podcast Title 5",
            by: "Author 5",
            episodes: episodes,
            rating: 4.2,
            reviews: 211,
            genre: "Genre 5",
            frequency: "Daily",
            backgroundColor: Color("bewjtColor"),
            backgroundIsDark: true
        ),
        Podcast(
            id: UUID(),
            image: UIImage(named: "onrac_logo")!,
            title: "Podcast Title 6",
            by: "Author 6",
            episodes: episodesRecent,
            rating: 4.2,
            reviews: 211,
            genre: "Genre 6",
            frequency: "Weekly",
            backgroundColor: Color("onracColor"),
            backgroundIsDark: true
        ),
        Podcast(
            id: UUID(),
            image: UIImage(named: "dgs_logo")!,
            title: "Podcast Title 7",
            by: "Author 7",
            episodes: episodes,
            rating: 4.2,
            reviews: 211,
            genre: "Genre 7",
            frequency: "Complete",
            backgroundColor: Color("dgsColor"),
            backgroundIsDark: false
        ),
    ]
}
