# Apple Podcasts Clone

## Project Overview

Apple Podcasts allows users to listen to podcasts and interact with a 
sophisticated UI to control their playback.

This clone does not.

Apple Podcasts Clone is an application built on the SwiftUI 
framework. It focuses on the `Library` "master-detail".

* ### :lipstick: The View
  As of the 21st of May 2023, it is a <em>façade</em>: only the
  View part of the project is serviceable.
  
  <img src="./docs/light_lib_up.png" height="700" style="margin:20px">
  <img src="./docs/light_lib_down.png" height="700" style="margin:20px">
  <br>
  <img src="./docs/light_pod_up.png" height="700" style="margin:20px">
  <img src="./docs/light_pod_down.png" height="700" style="margin:20px">
  <br>
  <img src="./docs/dark_lib_up.png" height="700" style="margin:20px">
  <img src="./docs/dark_lib_down.png" height="700" style="margin:20px">
  <br>
  <img src="./docs/dark_pod_up.png" height="700" style="margin:20px">
  <img src="./docs/dark_pod_down.png" height="700" style="margin:20px">
  
  <details><summary> more details... </summary>

  * #### Library ("master") 💐

    Users can browse a `Library` of `Podcasts` and select any podcast to inspect.

  * #### Podcast ("detail") 🌻

    Users can browse a `Podcast` of `Episodes`.

  * #### Dark/Light themes 🌙 ☀️

    This clone replicates the original dark/light themes by Apple Podcasts.

    To test this, you can change your device's (or your emulator's) display
setting to dark/light theme.

  * #### Top bar ☝️

    In Apple Podcasts, a stylish, slightly transparent top bar contains certain menu
options, and displays the name of the current
section once the user has scrolled past the corresponding header.

    This clone has no such thing.

  * #### Bottom bar 👇

    In Apple Podcasts, a consistent, stylish, slightly transparent bottom bar allows
navigation between different views, and playing songs.

    This clone tried to emulate that, but only the Library is there to visit, and the 
    "Now Playing" bar is static.
    
  </detail>

* ### :necktie: The Model

  A very basic Model is in place, to provide some sample data to views more easily.

## Installation

To run the Apple Podcasts Clone, you must use XCode -- your best bet is using a macOS machine.

Once in XCode, you can look through the `Views` -- they come with their own `Previews`. Find the `MainView`, 
and have a look around inside the preview. Sometimes, XCode is pretty neat. Sometimes...

## Some known limitations and shortcomings

Concerning the View part of this project:
* in the "detail" Podcast view, instead of saying "MORE", latest episode descriptions spanning more than 3 lines are truncated with a "..."
* throughout the app, the smart date formattings are close to the original app, but not quite as smart.
* the top bar was left unimplemented
* the "now playing bar" and the tabview don't have the same transparency / colors
* instead of i18, this clone has a very basic struct that provides static strings at need. Some string literals are still being used in the views.
* and many others will join this list, no doubt.
